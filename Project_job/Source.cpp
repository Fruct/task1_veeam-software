﻿#include <iostream>
#include "windows.h"
#include <fstream>
#include <tchar.h>
#include <tlhelp32.h>
#include <psapi.h>
#include <chrono>
#include <thread>


using namespace std;
template <typename F>
#pragma warning(disable : 4996)


void SetTimer(F&& func, size_t sec, PROCESS_INFORMATION pi)
{
    bool bExit = true;
    while (bExit) {
        switch (WaitForSingleObject(pi.hProcess, 0))
        {
        case WAIT_OBJECT_0:
            bExit = false;
            break;
        case WAIT_TIMEOUT:
            func();
            std::this_thread::sleep_for(std::chrono::seconds(sec));
            break;
        }

    }
}

unsigned long pid;
struct Statistics* info = 0;
int sponsorAmount = 0;

struct Statistics
{
    double CPU;
    double WS;
    double PB;
    DWORD hCount;
};

static ULARGE_INTEGER lastCPU, lastSysCPU, lastUserCPU;
static int numProcessors;
static HANDLE self;

void init(HANDLE hProc) {
    SYSTEM_INFO sysInfo;
    FILETIME ftime, fsys, fuser;

    GetSystemInfo(&sysInfo);
    numProcessors = sysInfo.dwNumberOfProcessors;

    GetSystemTimeAsFileTime(&ftime);
    memcpy(&lastCPU, &ftime, sizeof(FILETIME));

    self = GetCurrentProcess();
    GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);
    memcpy(&lastSysCPU, &fsys, sizeof(FILETIME));
    memcpy(&lastUserCPU, &fuser, sizeof(FILETIME));
    Sleep(100);
}

double getCurrentValue(HANDLE hProc) {
    FILETIME ftime, fsys, fuser;
    ULARGE_INTEGER now, sys, user;
    double percent;

    GetSystemTimeAsFileTime(&ftime);
    memcpy(&now, &ftime, sizeof(FILETIME));

    GetProcessTimes(self, &ftime, &ftime, &fsys, &fuser);
    memcpy(&sys, &fsys, sizeof(FILETIME));
    memcpy(&user, &fuser, sizeof(FILETIME));
    percent = (sys.QuadPart - lastSysCPU.QuadPart) +
        (user.QuadPart - lastUserCPU.QuadPart);
    percent /= (now.QuadPart - lastCPU.QuadPart);
    percent /= numProcessors;
    lastCPU = now;
    lastUserCPU = user;
    lastSysCPU = sys;
    return percent * 100;
}

Statistics* AddStruct(Statistics* Obj, const int amount)
{
    if (amount == 0)
        Obj = new Statistics[amount + 1]; 
    else {
        Statistics* tempObj = new Statistics[amount + 1];
        for (int i = 0; i < amount; i++){
            tempObj[i] = Obj[i]; 
        }
        delete[] Obj;
        Obj = tempObj;
    }
    return Obj;
}

void showData(const Statistics* Obj, const int amount)
{
    ofstream f;
    f.open("C:\\Users\\sarna\\OneDrive\\Desktop\\a.txt");
    f << "#\t " << "CPU%\t" << "Working Set\t" << "Private Bytes\t" << "OpenHanlde"<<"\n";
    f << fixed;
    f.precision(2);
    for (int i = 0; i < amount; i++){
        f << i << " \t" << Obj[i].CPU << "   \t" << Obj[i].WS << "     \t" << Obj[i].PB << "    \t" << Obj[i].hCount << "\n";
    }
    f.close();
}


void ram()
{
    HANDLE hProc = ::OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION | PROCESS_VM_READ, 0, pid);
    DWORD pidd = GetProcessId(hProc);
    DWORD hCount;
    info = AddStruct(info, sponsorAmount);
    GetProcessHandleCount(hProc, &hCount);
    init(hProc);
    double dd = getCurrentValue(hProc);
    info[sponsorAmount].CPU = dd;
    if (hProc)
    {
        PROCESS_MEMORY_COUNTERS_EX pmx = { 0 };
        if (::GetProcessMemoryInfo(hProc, (PROCESS_MEMORY_COUNTERS*)&pmx, sizeof(pmx)))
        {
            info[sponsorAmount].WS = pmx.WorkingSetSize / (1024.0 * 1024.0);
            info[sponsorAmount].PB = pmx.PrivateUsage / (1024.0 * 1024.0);
        }

        ::CloseHandle(hProc);
    }
    info[sponsorAmount].hCount = hCount;
    sponsorAmount++;
    
}

int main() {
    string proc_path;
    int timer;

    cout << "Enter path: ";
    cin >> proc_path;
    cout << "Enter time(sec): ";
    cin >> timer;
    cout << "\nPath - " << proc_path;
    cout << "\nTimer - " << timer;

    const char* tmp = proc_path.c_str();
    size_t wn = mbsrtowcs(NULL, &tmp, 0, NULL);
    wchar_t* buf = new wchar_t[wn + 1]();  
    wn = mbsrtowcs(buf, &tmp, wn + 1, NULL);

    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    if (!CreateProcess(NULL, buf,NULL,NULL,FALSE,0,NULL,NULL,&si, &pi))
    {
        printf("CreateProcess failed (%d).\n", GetLastError());
        return 0;
    }

    pid = pi.dwProcessId;
    SetTimer(ram, timer,pi);

    showData(info, sponsorAmount);

    delete[] info;
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    return 0;
}